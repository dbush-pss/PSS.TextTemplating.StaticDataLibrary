param($installPath, $toolsPath, $package, $project)
$projectFullName = $project.FullName
$projectName = $project.Name
$msg = "install.ps1 executing for " + $projectName
Write-Host $msg

# copy the language specific template, depending on the target project language
# I would have prefered to use language subfolders within the standard content folder,
# but apparently that only works if the project has a project.json file. <sigh>
# I'm cheating and using the target project file extension to determine if the target project uses VB or C#,
# since I could not find a better way to determine this. <more sighs>
$projectDirectory = Split-Path $projectFullName
$lang = $projectFullName.Substring($projectFullName.LastIndexOf(".") + 1, 2)
$sourceDirectory = "$installPath\content-$lang\StaticData"
$msg = "Copying files from $sourceDirectory to $projectDirectory\StaticData"
Write-Host $msg
# save a list of the template files that are copied, so that they can be manually added to the target project later
$templateFiles = Copy-Item $sourceDirectory -Recurse $projectDirectory -Force -PassThru

$lang = "any"
$sourceDirectory = "$installPath\content-$lang\StaticData"
$msg = "Copying files from $sourceDirectory to $projectDirectory\StaticData"
Write-Host $msg
$staticFiles = Copy-Item $sourceDirectory -Recurse $projectDirectory -Force -PassThru

# copy assembly references
$sourceDirectory = "$installPath\lib\net45"
$projectOutputFolder = ($project.ConfigurationManager.ActiveConfiguration.Properties | Where Name -match OutputPath).Value
$destinationDirectory = "$projectDirectory\$projectOutputFolder"

if(Test-Path $sourceDirectory -PathType container)
{
	$msg = "Copying files from $sourceDirectory to $destinationDirectory"
	Write-Host $msg

	$files = Get-ChildItem "$sourceDirectory" -filter *.dll |
	Foreach-Object {
		$fileName = $_.Name
		$destPath = "$destinationDirectory\$fileName"

		if (!(Test-Path $destPath -PathType Leaf))
		{
			Write-Host $fileName
			Copy-Item $_.FullName $destPath
		}
	}
}

# include the template files in the project
# one benefit to manually adding the template files to the project is that it seems to avoid the situation where
# the template is compiled "too quickly" before the assembly references are resolved, which would result in a compile-time error.
# I had resorted to using a different file extension for the templates, and then renaming the files manually,
# which also got around the issue. But that also led to other nuances, such as the generated file having a number appended to the filename, etc.
# so, I'm happy to eliminate the file renames.
foreach($file in $templateFiles) {
	if (!($file.FullName.EndsWith("StaticData")))
	{
		$project.ProjectItems.AddFromFile($file.FullName)
	}
}
foreach($file in $staticFiles) {
	if (!($file.FullName.EndsWith("StaticData")))
	{
		$project.ProjectItems.AddFromFile($file.FullName)
	}
}

# the rename is no longer necessary - see commentary above
#$templateFile = $project.ProjectItems.Item("StaticData").ProjectItems.Item("StaticDataLibrary.t4t")
#$templateFile.Name = "StaticDataLibrary.tt"

$msg = "install.ps1 complete for " + $projectName
Write-Host $msg
