param($installPath, $toolsPath, $package, $project)

$projectFullName = $project.FullName
$msg = "uninstall.ps1 executing for " + $project.Name
Write-Host $msg

$projectDirectory = Split-Path $project.FullName
$sourceDirectory = "$installPath\lib\net45"
$projectOutputFolder = ($project.ConfigurationManager.ActiveConfiguration.Properties | Where Name -match OutputPath).Value
$destinationDirectory = "$projectDirectory\$projectOutputFolder"

$files = Get-ChildItem "$sourceDirectory" -filter *.dll |
Foreach-Object {
    $fileName = $_.Name
	$fullPath = "$destinationDirectory\$fileName"

	if (Test-Path $fullPath -PathType Leaf)
	{
		$msg = "Deleting $fullPath"
		Write-Host $msg
		Remove-Item $fullPath
	}
}

$msg = "Removing StaticDataLibrary.tt from " + $project.Name
Write-Host $msg

$project.ProjectItems | where { $_.Name -eq "StaticData" } |
ForEach { $_.ProjectItems } | 
	where { $_.Name -eq "StaticDataLibrary.tt" -or  $_.Name -eq "readme.txt" } | 
		%{ $_.Remove() }

$msg = "uninstall.ps1 complete for " + $project.Name
Write-Host $msg

