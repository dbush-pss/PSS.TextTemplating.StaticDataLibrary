﻿The StaticDataLibrary template generates a library of static data objects from CSV and Excel files. The primary intended use for this template is to generate lists of data
at design-time for unit tests, although it can also be used to generate other static data that doesn't change during application use, such as a list of states, etc.

Developers often use CSV and other types of formatted data in their unit tests. However, as a best practice and to keep tests performing well, 
ideally unit tests do not make use of the file system during test execution. If you have hundreds of tests, it can quickly become cumbersome
to write and maintain code to manually create test data. We typically resort to copy/paste, or perhaps generating the code using Excel formulas or SQL scripts.
The latter approaches at least keeps our test data in a manageable data store, although they're hardly ideal code generators.
This solution attempts to keep test data manageable in a logical folder/file structure, while using a T4 template for code generation.

** Note: ** This package currently only provides a build for .NET 4.5 or later. If there is a need for a .NET 4.0 build, I can create one if requested.
I'll be working on a version for .NET Core at some point.

To keep things somewhat simple, there are some conventions that you need to follow, but hopefully you'll find these helpful rather than restrictive.
1) Organize test data using a folder structure that mimics your object namespaces and the classes contained therein. 
   The top-level folders represent namespaces, and each sub-folder represents a class name. 
   Optionally, the class sub-folders can be further organized into as many child sub-folders as desired.
   All data files contained within the class folders or any child sub-folders are treated as test data, provided that they end with one of these file extensions: .csv, .xls, .xlsx.
   Code examples are found later below.

2) The first row in each file MUST be a header line that contains the property names, with correct casing (c#), etc. 
   Subsequent rows are data rows that correspond to the property names defined in the header.

   Starting with v2.0, you can now have columns that do not correspond to a property of the object being created. These colums are treated as comments, and will be output as such in the generated code.

3) Data formatting:
   In v1.x, there was no type discovery, which meant you had to format the data rather precisely in order to generate valid code.
   For backwards-compatibility, this mode is still supported, although I'm sure most will prefer the newer method that doesn't require type suffixes for numeric types.
   The updated T4 template sets the property "UseVsCodeModelForCsvFiles" to true, which indicates the newer method of code generation will be used for CSV files.
   Using an older v1.x template that doesn't contain this property assignment will invoke the older code generation method requiring the stricter formatting rules.
   Alternatively, you can just modify the new T4 template to set the UseVsCodeModelForCsvFiles to false to preserve the older formatting method.
   
   Note: If you want to use existing CSV files that were already formatted for v1.x, but want to use the newer formatting method going forward in the same project, 
         you can create separate root folders for each set of files.
   
   Formatting rules (v2.0+):
   a) Data simply needs to match the corresponding property type.
      - If a data value cannot be converted to the corresponding property type (i.e. invalid date or numeric data), 
        a comment is generated in the code in place of the property assignment, and a warning is displayed in the Error List window.
      - Strings can be enclosed in quotes, but this is not required. However, if you need to preserve leading/trailing spaces, you must enclose the string in quotes.
      - Dates can be wrapped in hash tags (CSV files only), but this also is no longer required. Do not surround dates with hash tags in Excel files.
      - The shortcuts for DateTime.MinValue or DateTime.MaxValue are still valid in both CSV and Excel files. Use ## or ####, respectively.
      - In CSV files, spaces after each comma delimiter are optional. It will work with or without them.
      - In CSV files, embedded commas in text fields are not supported. If you really need an embedded comma in a string property for a particular scenario, 
        you must manually update the data in your test case prior to using the data.

   b) Blank rows are supported in both file types.

   c) Comments are supported in multiple ways:
      - As mentioned earlier, any column heading that doesn't correspond to a valid property type is treated as a "comment" column.
      - The column heading and value will be written to the output as a comment in place of a property assignment. 
        In VB code, all of the comment columns are grouped together following the object creation, to preserve proper syntax.
      - Comment rows can be added in both CSV and Excel files:

        CSV files follow the same convention as v1.x, using either single or multi-line comment markers:
          // test case 1: happy path
          1, "Foo", 1.5, ...

          // test case 2: out of bounds
          2, "Foo", 500.0, ...

          /* edge case: multiple conditions */
          3, "Some bad data", 355.9, ...

        Note the two different styles of comments that are supported (// and /* ... */)

        For Excel files, put two asterisks(**) in column A, and your comment in column B.

      - Comments are also written into the generated source code, which can help when you are debugging tests that may not produce the expected results.

   Formatting rules (v1.x) - applies only to CSV files, and only when UseVsCodeModelForCsvFiles = false in the T4 template (or if no property assignment exists) :
   a) Wrap string data in double quotes, but do not wrap numeric data in quotes.
   b) Wrap dates in hash tags (#5/15/2016# or #5/14/2016 11:25:45PM#)
   c) For DateTime.MinValue or DateTime.MaxValue, you can use the shortcuts ## or ####, respectively.
   d) Use type suffixes as necessary, depending on your target programming language.
      For example, decimal types use 1.5m (C#) or 1.5D (VB)
   e) Spaces after each comma delimiter are optional. It will work with or without them.
   f) Blank lines and "comment" lines are supported. For example, to annotate your test scenarios, you can precede any row of data with a comment, as follows:
      // test case 1: happy path
      1, "Foo", 1.5m, ...

      // test case 2: out of bounds
      2, "Foo", 500.0m, ...

      /* edge case: multiple conditions */
      3, "Some bad data", 355.9m, ...

      Note the two different styles of comments that are supported (// and /* ... */)
      Comments are also written into the generated source code, which can help when you are debugging tests that may not produce the expected results.
   g) Embedded commas in text fields are not supported. If you really need an embedded comma in a string property for a particular scenario, 
      you must manually update the data in your test case prior to using the data.

Example CSV file (for v2.0+):
Id,Name,BookValue, ProcessDate,LastUpdated
1, "Foo", 19939.59m, #5/15/2016#, #5/14/2016 11:25:45AM#
2, Bar, 34153.33, 5/15/2016, 5/14/2016 11:25:45PM
3, Another, 423048.4334, ##, 5/14/2016 11:25:45PM

Example CSV file (for v1.x):
Id,Name,BookValue, ProcessDate,LastUpdated
1, "Foo", 19939.59m, #5/15/2016#, #5/14/2016 11:25:45AM#
2, "Bar", 34153.33m, #5/15/2016#, #5/14/2016 11:25:45PM#
3, "Another", 423048.4334m, ##, #5/14/2016 11:25:45PM#

*****************************
** Data File Folder Structure
*****************************
Data files should be placed in subfolders below the folder where this template
  resides, using this structure:
Template Folder\
  Namespace1\
    ClassName1\
      File1.csv
      File2.csv
      File3.xlsx
    ClassName2\
      File1.csv
      File2.csv
      File2.xls
      File3.csv
  Namespace2\
    ClassName2\
      File1.csv

Additionally, files below each ClassName folder can be organized into subfolders if desired. 
Note that while the same filename can be used for multiple classes, you should not use the same filename within a single class's subfolders.

For example, the following structure works fine:
Acme.App.Core\Widget\
   File1.csv
   File2.csv
Acme.App.Core\Hammer\
   File1.csv
   File2.csv

But this structure would not work (because the Hammer class would have duplicate filenames):
Acme.App.Core\Widget\
   File1.csv
   File2.csv
Acme.App.Core\Hammer\Scenario1
   File1.csv
   File2.csv
Acme.App.Core\Hammer\Scenario2
   File1.csv
   File2.csv

The duplicate files are ignored, and a corresponding warning is displayed in the output window.
A comment is also written to the generated code file.

*******************************
** Using the data in unit tests
*******************************
Typically, the data will be used in mock data access calls, as follows:

StaticDataLibrary library = StaticDataLibrary.Current;
var input = library.GetList<Widget>("happy-path.csv");
var dataAccess = new Mock<IDataAccess>();
dataAccess.Setup(m => m.GetWidgets("param value")).Returns(input);

Note that in the code above, the "happy-path.csv" file is not actually opened or read at the time of the test execution. 
It is only used as an identifier to return the appropriate set of data in the form of a 'List<Widget>'.  
This convention can be useful in keeping unit test code maintainable, provided that you name your test data files appropriately.

********************
** Special Use Cases
********************
1) It's possible to have the same class name reside under multiple namespaces. However, the generated code must fully qualify each type in this scenario.
   You will need to modify the T4 template, by setting the UseFullyQualifiedTypeNames property to true. This line is found near the top of the template:
   UseFullyQualifiedTypeNames = false;  (C# example)
   
2) By default, the T4 template uses 4 spaces for code indentation. This can also be changed to use a tab or a different number of spaces 
   by changing this line of code, found near the top of the T4 template:
   //SingleIndent = "\t";   (C# example)

3) If you remove the NuGet package, the T4 template and any generated code are removed from your project.
   If you need to preserve the generated code, be sure to either rename it or copy it to a different folder.
