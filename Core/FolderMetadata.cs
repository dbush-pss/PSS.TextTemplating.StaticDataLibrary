﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSS.TextTemplating.Core
{
    public class FolderMetadata
    {
        public string[] DiscoveredNamespaces { get; private set; }

        public List<DirectoryInfo> ClassFolders { get; private set; }

        public FolderMetadata(string[] discoveredNamespaces, List<DirectoryInfo> classFolders)
        {
            DiscoveredNamespaces = discoveredNamespaces;
            ClassFolders = classFolders;
        }
    }
}
