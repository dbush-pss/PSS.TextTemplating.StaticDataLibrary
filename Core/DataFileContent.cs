﻿using System.Collections.ObjectModel;

namespace PSS.TextTemplating.Core
{
    /// <summary>
    /// Data structure that represents the data from any type of input file (CSV or Excel)
    /// </summary>
    public class DataFileContent
    {
        /// <summary>
        /// Gets or sets a value that represents the full path of the file containing the data for this instance
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// The fully-qualified name of the class that will be created using data in this instance
        /// </summary>
        public string ClassFullName { get; set; }

        /// <summary>
        /// Collection of column header names that are created from the first row in the data file
        /// </summary>
        public ColumnHeaderCollection ColumnHeaders { get; set; }

        /// <summary>
        /// Collection of rows containing the data for each object to be created
        /// </summary>
        public Collection<DataValues> Rows { get; set; }
        
        public DataFileContent(string filePath, string classFullName)
        {
            FilePath = filePath;
            ClassFullName = classFullName;

            ColumnHeaders = new ColumnHeaderCollection();
            Rows = new Collection<DataValues>();
        }
    }
}
