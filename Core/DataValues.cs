﻿using System;
using System.Collections.Generic;

namespace PSS.TextTemplating.Core
{
    public class DataValues
    {
        private Dictionary<int, string> values = new Dictionary<int, string>();

        public string CommentText { get; set; }

        public DataValues()
        {
            CommentText = string.Empty;
        }

        public string this[int index]
        {
            get
            {
                if (values.ContainsKey(index))
                {
                    return values[index];
                }
                throw new ArgumentOutOfRangeException();
            }

            set
            {
                if (values.ContainsKey(index))
                {
                    values[index] = value;
                }
                else
                {
                    values.Add(index, value);
                }
            }
        }

        /// <summary>
        /// Populate the Values collection from a string array
        /// </summary>
        /// <param name="values"></param>
        public void FromArray(string[] values)
        {
            Clear();
            for (int i = 0; i < values.Length; i++)
            {
                this[i] = values[i];
            }

        }

        /// <summary>
        /// Removes all values from the collection
        /// </summary>
        public void Clear()
        {
            values.Clear();
        }

        /// <summary>
        /// Gets a value indicating how many items belong to the values collection
        /// </summary>
        public int Count
        {
            get
            {
                return values.Count;
            }
        }

        
    }
}
