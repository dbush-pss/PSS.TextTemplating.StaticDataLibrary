﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text.RegularExpressions;

namespace PSS.TextTemplating.Core
{
    /// <summary>
    /// Adapter class that converts raw data into a DataTable that represents the properties of an object and its data that is converted to the correct type for each property.
    /// </summary>
    public class ContentAdapter
    {
        private readonly SchemaLibrary schemaLibrary;
        private readonly CodeDomProvider codeDomProvider;

        #region Constants

        private const string nullValue = "null";
        private const string quote = "\"";
        private const string underscore = "_";
        private const string dateMarker = "#";
        private const string minDate = "##";
        private const string maxDate = "####";
        private const string dateType = "DateTime";
        private const string illegalDataColumnNameCharacters = @"[\w\r\n\[\]\.\\()/]";
        private const string dataRowExceptions = "dataRowExceptions";
        private const string excludeFromCodeGenProp = "ExcludeFromCodeGen";
        private const string emptyRow = "EmptyRow";
        private const string column = "Column";

        #endregion

        #region Constructors

        public ContentAdapter(CodeDomProvider codeDomProvider)
        : this(codeDomProvider, null)
        {
            IgnoreSchemaLibrary = true;
        }

        public ContentAdapter(CodeDomProvider codeDomProvider, SchemaLibrary schemaLibrary)
        {
            this.codeDomProvider = codeDomProvider;
            this.schemaLibrary = schemaLibrary;
        }

        #endregion

        public bool IgnoreSchemaLibrary { get; private set; }

        /// <summary>
        /// Convert the content of a data file into a DataTable, using the SchemaLibrary that contains the type information for all objects in the current solution.
        /// </summary>
        /// <param name="dataFileContent"></param>
        /// <returns></returns>
        public DataTable CreateDataTable(DataFileContent dataFileContent)
        {
            DataTable dt = null;

            if (dataFileContent.ColumnHeaders != null && dataFileContent.ColumnHeaders.Count > 0)
            {
                dt = CreateSchema(dataFileContent);
                Fill(dt, dataFileContent.Rows, dataFileContent);
            }
            else
	        {
                throw new Exception(string.Format(CoreResources.ErrorDataFileHasNoColumnHeadings, dataFileContent.FilePath));
	        }

            return dt;
        }

        private DataTable CreateSchema(DataFileContent dataFileContent)
        {
            DataTable sourceDataTable = null;
            if (schemaLibrary != null)
            {
                if (schemaLibrary.ContainsKey(dataFileContent.ClassFullName))
                {
                    sourceDataTable = schemaLibrary[dataFileContent.ClassFullName];
                }
                else
                {
                    throw new Exception(string.Format(CoreResources.ErrorUnableToDiscoverType, dataFileContent.ClassFullName));
                }
            }

            var dt = new DataTable();

            bool hasMatchingHeaders = false;

            foreach (var columnName in dataFileContent.ColumnHeaders)
            {
                var colType = typeof(string);
                var excludeFromCodeGen = false;
                var allowDBNull = true;
                var colName = columnName.Trim();
                var colCaption = colName;

                if (schemaLibrary == null)
                {
                    if (codeDomProvider.IsValidIdentifier(colName))
                    {
                        hasMatchingHeaders = true;
                    }
                    else
                    {
                        excludeFromCodeGen = true;
                    }
                }
                else
                {
                    if (sourceDataTable.Columns.Contains(colName))
                    {
                        var prop = sourceDataTable.Columns[colName];
                        hasMatchingHeaders = true;
                        colType = prop.DataType;
                        allowDBNull = prop.AllowDBNull;
                    }
                    else
                    {
                        excludeFromCodeGen = true;
                    }
                }

                if (excludeFromCodeGen)
                {
                    colName = Regex.Replace(colName, illegalDataColumnNameCharacters, underscore);   // normalize the column name to be on the safe side
                }

                if (dt.Columns.Contains(colName))
                {
                    colName += "_" + Guid.NewGuid();    // quick and dirty way of dealing with unlikely duplicate column names
                }

                var col = dt.Columns.Add(colName, colType);
                col.Caption = colCaption;
                col.AllowDBNull = allowDBNull;
                col.ExtendedProperties.Add(excludeFromCodeGenProp, excludeFromCodeGen);
            }

            if (!hasMatchingHeaders)
            {
                var msg = schemaLibrary == null ? string.Format(CoreResources.ErrorInvalidHeaderRow, dataFileContent.FilePath)
                    : string.Format(CoreResources.ErrorHeaderRowHasNoMatchingProps, dataFileContent.FilePath, dataFileContent.ClassFullName);
                throw new Exception(msg);
            }

            return dt;
        }

        private void Fill(DataTable dt, Collection<DataValues> content, DataFileContent dataFileContent)
        {
            dt.Clear();
            var exceptionMsgs = new List<string>();

            for (int rowIndex = 0; rowIndex < content.Count; rowIndex++)
            {
                DataValues rowData = content[rowIndex];
                var row = dt.NewRow();

                if (rowData.Count > 0)
                {
                    if (schemaLibrary == null)
                    {
                        if (rowData.Count != dt.Columns.Count)
                        {
                            FillEmptyRow(dt, row, string.Format(CoreResources.ErrorDataElementMismatch, rowIndex + 2, dataFileContent.FilePath));
                            continue;
                        }
                    }

                    FillRow(dt, row, rowData);
                }
                else if (!string.IsNullOrWhiteSpace(rowData.CommentText))
                {
                    FillEmptyRow(dt, row, rowData.CommentText.Trim());
                }
                else
                {
                    FillEmptyRow(dt, row, emptyRow);
                }

                try
                {
                    dt.Rows.Add(row);
                }
                catch (Exception ex)
                {
                    exceptionMsgs.Add(string.Format(CoreResources.ErrorAddingDataRow, rowIndex + 2, ex.Message));
                }
            }

            dt.ExtendedProperties.Add(dataRowExceptions, exceptionMsgs);
        }

        private void FillRow(DataTable dt, DataRow row, DataValues rowData)
        {
            if (schemaLibrary != null)
            {
                ExtendColumnsToFitData(dt, rowData);
            }

            for (int i = 0; i < rowData.Count; i++)
            {
                var value = rowData[i].Trim();
                if (value.Length > 0 && !value.Equals(nullValue, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (schemaLibrary == null)
                    {
                        row[i] = value;
                    }
                    else
                    {
                        value = StripQuotes(value);

                        try
                        {
                            if (dt.Columns[i].DataType.Name == dateType && value.StartsWith(dateMarker))
                            {
                                row[i] = TryParseDate(value);
                            }
                            else
                            {
                                row[i] = Convert.ChangeType(value, dt.Columns[i].DataType);
                            }
                        }
                        catch (Exception ex)
                        {
                            row.SetColumnError(i, string.Format(CoreResources.ErrorDataTypeMismatch, dt.Columns[i].Caption, value, ex.Message));
                        }
                    }

                }
                else if (!dt.Columns[i].AllowDBNull)
                {
                    row[i] = schemaLibrary.GetDefaultValue(dt.Columns[i].DataType);
                }
            }
        }

        private static string StripQuotes(string value)
        {
            if (value.StartsWith(quote))
            {
                value = value.Substring(1);
            }
            if (value.EndsWith(quote))
            {
                value = value.Substring(0, value.Length - 1);
            }

            return value;
        }

        private static void ExtendColumnsToFitData(DataTable dt, DataValues rowData)
        {
            while (rowData.Count > dt.Columns.Count)
            {
                var colName = string.Concat(column, dt.Columns.Count + 1);
                var col = dt.Columns.Add(colName, typeof(string));
                col.Caption = colName;
                col.AllowDBNull = true;
                col.ExtendedProperties.Add(excludeFromCodeGenProp, true);
            }
        }

        private void FillEmptyRow(DataTable dt, DataRow row, string rowErrorText)
        {
            row.RowError = rowErrorText;
            foreach (DataColumn col in dt.Columns)
            {
                if (!col.AllowDBNull)
                {
                    row[col] = schemaLibrary.GetDefaultValue(col.DataType);
                }
            }
        }

        private DateTime TryParseDate(string value)
        {
            DateTime result = DateTime.MinValue;
            if (value.StartsWith(dateMarker))
            {
                if (value.Equals(minDate))
                {
                    return DateTime.MinValue;
                }
                if (value.Equals(maxDate))
                {
                    return DateTime.MaxValue;
                }

                if (value.Length > 4 && value.EndsWith(dateMarker))
                {
                    return DateTime.Parse(value.Replace(dateMarker, string.Empty));
                }
                throw new Exception(CoreResources.ErrorInvalidDateValue);
            }
            return result;
        }
    }
}
