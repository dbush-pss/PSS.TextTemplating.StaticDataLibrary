﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using EnvDTE;

namespace PSS.TextTemplating.Core
{
    public class SchemaLibrary : Dictionary<string, DataTable>
    {
        private const string system = "System";
        private const string sysNullablePrefix = "System.Nullable";
        private static readonly Regex nullableRegex = new Regex(@"^System.Nullable(<|\(Of )(?<T>.*)(>|\))$");

        private readonly CodeDomProvider codeDomProvider;
        private readonly Project hostProject;
        private readonly string[] targetNamespaces;
        private Dictionary<string, object> typeDefaultValues = new Dictionary<string, object>();
        private readonly bool debug;

        public SchemaLibrary(CodeDomProvider codeDomProvider, Project hostProject, string[] targetNamespaces)
            : this(codeDomProvider, hostProject, targetNamespaces, false)
        {
        }
        public SchemaLibrary(CodeDomProvider codeDomProvider, Project hostProject, string[] targetNamespaces, bool debug)
        {
            if (codeDomProvider == null) throw new ArgumentNullException("codeDomProvider");
            if (hostProject == null) throw new ArgumentNullException("hostProject");
            if (targetNamespaces == null) throw new ArgumentNullException("targetNamespaces");
            this.codeDomProvider = codeDomProvider;
            this.hostProject = hostProject;
            this.targetNamespaces = targetNamespaces;
            this.debug = debug;

            Build();
        }

        public List<string> DebugStatements { get; private set; }

        private void Build()
        {
            DebugStatements = new List<string>();

            var codeElements = hostProject.CodeModel.CodeElements;
            var codeNamespaces = codeElements
                .OfType<EnvDTE.CodeNamespace>()
                .Where(IsNotExcludedNamespace());

            foreach (var ns in codeNamespaces)
            {
                DiscoverType(ns as CodeElement, targetNamespaces);
            }
        }

        private void DiscoverType(CodeElement codeElement, string[] targetNamespaces)
        {
            if (codeElement.Kind == vsCMElement.vsCMElementNamespace)
            {
                // Keep drilling down on the namespaces until we find classes
                var ns = codeElement as EnvDTE.CodeNamespace;
                if (ns != null)
                {
                    foreach (CodeElement childElement in ns.Members)
                    {
                        DiscoverType(childElement, targetNamespaces);
                    }
                }
            }
            else if (codeElement.Kind == vsCMElement.vsCMElementClass)
            {
                var codeClass = codeElement as CodeClass;
                if (codeClass != null && codeClass.Namespace != null && targetNamespaces.Contains(codeClass.Namespace.FullName))
                {
                    DiscoverProperties(codeClass);
                }
            }
        }

        private void DiscoverProperties(CodeClass codeClass)
        {
            DataTable dt = new System.Data.DataTable(codeClass.FullName);
            foreach (CodeProperty prop in codeClass.Members.OfType<CodeProperty>())
            {
                string typeName = prop.Type.AsFullName;
                if (prop.Type.AsFullName.StartsWith(system))
                {
                    bool isNullable = false;
                    Type type = null;
                    if (typeName.StartsWith(sysNullablePrefix))
                    {
                        AddDebugStatement("Getting underlying type for " + typeName);
                        typeName = ExtractUnderlyingType(typeName);
                        isNullable = true;
                    }
                    if (!string.IsNullOrEmpty(typeName))
                    {
                        type = Type.GetType(typeName);
                    }

                    if (type != null)
                    {
                        dt.Columns.Add(prop.Name, type).AllowDBNull = isNullable || type == typeof(string);
                    }
                    else
                    {
                        AddDebugStatement("Unable to determine type for " + typeName);
                    }
                }
            }

            Add(codeClass.FullName, dt);
        }

        private static Func<EnvDTE.CodeNamespace, bool> IsNotExcludedNamespace()
        {
            var excludedNamespaces = new string[] { "mscorlib", "Excel", "EnvDTE", "Moq", "ICSharpCode", "Castle" };
            return x => !x.Name.StartsWith("System") && !x.Name.StartsWith("Microsoft") && !x.Name.StartsWith("Windows") && !excludedNamespaces.Contains(x.Name);
        }

        public object GetDefaultValue(Type type)
        {
            if (!typeDefaultValues.ContainsKey(type.Name))
            {
                typeDefaultValues.Add(type.Name, CreateDefaultInstance(type));
            }
            return typeDefaultValues[type.Name];
        }

        private object CreateDefaultInstance(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        private string ExtractUnderlyingType(string typeName)
        {
            // CodeTypeRef does not expose generic type args directly, hence falling back on Regex match
            var match = nullableRegex.Match(typeName);
            if (match.Success)
            {
                var result = match.Groups["T"].Value;
                AddDebugStatement("Underlying type is " + result);
                return result;
            }
            AddDebugStatement("No Regex match for " + typeName ?? "null");

            return string.Empty;
        }

        private Type ResolveType(string typeName, bool isNullable)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                return null;
            }
            CodeTypeReference ctr;

            if (isNullable)
            {
                ctr = new CodeTypeReference(typeof(Nullable<>));
                ctr.TypeArguments.Add(new CodeTypeReference(typeName));
            }
            else
            {
                ctr = new CodeTypeReference(typeName);
            }

            var resolvedTypeName = codeDomProvider.GetTypeOutput(ctr);
            AddDebugStatement("Resolved type name: " + resolvedTypeName ?? "null");
            return Type.GetType(resolvedTypeName);
        }

        private void AddDebugStatement(string text)
        {
            if (debug)
            {
                DebugStatements.Add(text);
            }
        }
    }
}
