﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSS.TextTemplating.Core
{
    /// <summary>
    /// Encapsulates information regarding the object that is being generated
    /// </summary>
    public class ModelInfo
    {
        private const string prefix = "list";
        private const string dot = ".";

        /// <summary>
        /// The name of the class, without the namespace
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The class name used for generating code, taking into account if the fully-qualified name should be used
        /// </summary>
        public string OutputName { get; private set; }

        /// <summary>
        /// The full-qualified name of the class, including the namespace
        /// </summary>
        public string FullName { get; private set; }

        /// <summary>
        /// The name of the local variable to use for generating code
        /// </summary>
        public string LocalVariableName { get; private set; }

        /// <summary>
        /// The list of content files to use for code generation for this class
        /// </summary>
        public IEnumerable<FileInfo> ContentFiles { get; private set; }

        public ModelInfo(string namespaceName, string className, bool useFullyQualifiedTypeNames, IEnumerable<FileInfo> contentFiles)
        {
            Name = className;
            FullName = string.Concat(namespaceName, dot, Name);
            LocalVariableName = string.Concat(prefix, Name);
            OutputName = useFullyQualifiedTypeNames ? FullName : Name;
            ContentFiles = contentFiles;
        }
    }
}
