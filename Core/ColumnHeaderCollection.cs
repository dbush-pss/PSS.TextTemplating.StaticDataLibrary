﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PSS.TextTemplating.Core
{
    public class ColumnHeaderCollection : IEnumerable<string>
    {
        private Dictionary<int, string> columns = new Dictionary<int, string>();

        public string this[int index]
        {
            get 
            {
                if (columns.ContainsKey(index))
                {
                    return columns[index];
                }
                throw new ArgumentOutOfRangeException("index");
            }
            
            set 
            {
                if (columns.ContainsKey(index))
                {
                    columns[index] = value;
                }
                else
                {
                    columns.Add(index, value);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating how many column headers are currently in the collection
        /// </summary>
        public int Count
        {
            get
            {
                return columns.Count;
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        public IEnumerator<string> GetEnumerator()
        {
            for (int i = 0; i < columns.Count; i++)
            {
                yield return columns[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
