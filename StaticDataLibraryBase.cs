﻿using System;
using System.Collections.Generic;

namespace PSS.TextTemplating
{
    /// <summary>
    /// Base class that provides convenient access to a library of generated static data.
    /// </summary>
    public abstract class StaticDataLibraryBase
    {
        private const string colon = ":";
        protected Dictionary<string, object> listLibrary = new Dictionary<string, object>();

        /// <summary>
        /// Return a list of the specified type of objects that was generated using data in a CSV or Excel file 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> GetList<T>(string fileName)
        {
            if (fileName == null) throw new ArgumentNullException(nameof(fileName));

            var key = string.Concat(typeof(T).FullName, colon, fileName);
            if (listLibrary.ContainsKey(key))
            {
                return listLibrary[key] as List<T>;
            }
            else
            {
                return new List<T>();
            }
        }
    }
}
