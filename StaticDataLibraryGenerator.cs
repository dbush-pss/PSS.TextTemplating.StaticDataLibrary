﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Resources;
using System.Text;
using EnvDTE;
using Microsoft.VisualStudio.TextTemplating;
using PSS.TextTemplating.Core;
using PSS.TextTemplating.IO;

namespace PSS.TextTemplating
{
    /// <summary>
    /// Base class for T4 template that generates a static data library
    /// </summary>
    public abstract class StaticDataLibraryGenerator : TextTransformation
    {
        #region Private Members

        private readonly CodeDomProvider codeDomProvider;
        private static ResourceManager resourceManager;
        private ITextTemplatingEngineHost _host;
        private Project _project;
        private FileService fileService = null;
        private FolderMetadata folderMetadata = null;
        private SchemaLibrary schemaLibrary = null;

        #endregion

        #region Constants

        public const string langConstVB = "VisualBasic";
        public const string langConstCS = "CSharp";

        private const string csvExtension = ".csv";
        private const string openBrace = "{";
        private const string closeBrace = "}";
        private const string listVarPrefix = "list";
        private const string closeObject = "})";
        private const string defaultIndent = "    ";
        private const string dateMarker = "#";
        private const string minDate = "##";
        private const string maxDate = "####";
        private const string dateType = "DateTime";
        private const string stringType = "String";
        private const string quote = "\"";
        private const string comma = ",";
        private const string dot = ".";
        private const string colon = ":";
        private const string nullValue = "null";
        private const string minValue = "MinValue";
        private const string projectDefaultNamespace = "projectDefaultNamespace";
        private const string host = "Host";
        private const string excludeFromCodeGenProp = "ExcludeFromCodeGen";
        private const string emptyRow = "EmptyRow";
        private const string dataRowExceptions = "dataRowExceptions";

        private const string resKeyUsing = "Using";
        private const string resKeyBeginRegion = "BeginRegion";
        private const string resKeyEndRegion = "EndRegion";
        private const string resKeyNewListDeclaration = "NewListDeclaration";
        private const string resKeyNewListInstantiation = "NewListInstantiation";
        private const string resKeyAddListLibrary = "AddListLibrary";
        private const string resKeyAddNewObjectStarterCode = "AddNewObjectStarterCode";
        private const string resKeyCodeLineTerminator = "CodeLineTerminator";
        private const string resKeyNullValueIdentifier = "NullValueIdentifier";
        private const string resKeyPropAssignment = "PropAssignment";
        private const string resKeyDateParse = "DateParse";
        private const string resKeyDateMinValue = "DateMinValue";
        private const string resKeyDateMaxValue = "DateMaxValue";
        private const string resKeyCastValue = "CastValue";

        private const string resKeyCommentInitiator = "CommentInitiator";
        private const string resKeyCommentPopulateList = "CommentPopulateList";
        private const string resKeyCommentEmptyFile = "CommentEmptyFile";
        private const string resKeyCommentErrorGeneratingList = "CommentErrorGeneratingList";
        private const string resKeyCommentNoDataFilesFound = "CommentNoDataFiles";
        private const string resKeyCommentUnableToGenerateRegion = "CommentUnableToGenerateRegion";

        private const string resKeyWarningMissingQuote = "WarningMissingQuote";
        private const string resKeyWarningInvalidDateFormat = "WarningInvalidDateFormat";
        private const string resKeyWarningProcessingRow = "WarningProcessingRow";

        private const string vbCommentInitiator = "'";

        #endregion

        #region Constructor

        public StaticDataLibraryGenerator()
        {
            SingleIndent = defaultIndent;
            codeDomProvider = CodeDomProvider.CreateProvider(CodeLanguage);
            UseVsCodeModelForCsvFiles = false;  // For backwards-compatibility with v1.x so older templates will continue to work. The updated T4 template sets this property to true, however.
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// CodeLanguage is implemented by each language-specific T4 template
        /// </summary>
        protected abstract string CodeLanguage { get; }

        /// <summary>
        /// Gets or sets a value that indicates if code generation from CSV files should use the Visual Studio code model for type resolution.
        /// </summary>
        public bool UseVsCodeModelForCsvFiles { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates if object names in the generated class will be fully qualified. The default is false, which results in Using statements being added to the top of the generated file.
        /// </summary>
        public bool UseFullyQualifiedTypeNames { get; set; }

        /// <summary>
        /// Gets or sets a value that represents a single code indention block. The default value is four spaces, but this can be changed to a tab by setting the value to "\t".
        /// </summary>
        public string SingleIndent { get; set; }

        public bool DebugSchemaDiscovery { get; set; }

        #endregion


        /// <summary>
        /// Initialize the templating class
        /// </summary>
        public override void Initialize()
        {
            try
            {
                base.Initialize();

                if (HostProject != null)
                {
                    fileService = CreateFileService();
                    folderMetadata = fileService.GetFolderMetadata();
                    schemaLibrary = new SchemaLibrary(codeDomProvider, HostProject, folderMetadata.DiscoveredNamespaces, DebugSchemaDiscovery);
                }
                else
                {
                    throw new Exception(CoreResources.ErrorHostNotAvail);
                }
            }
            catch (Exception ex)
            {
                Error(string.Format(CoreResources.ErrorTemplateInitialization, ex.ToString()));
            }
        }

        /// <summary>
        /// Primary method used to generate all static data objects based on the previously discovered file structure
        /// </summary>
        protected void BuildStaticDataContent()
        {
            if (DebugSchemaDiscovery)
            {
                schemaLibrary.DebugStatements.ForEach(comment => WriteComment(comment));
            }

            if (folderMetadata.DiscoveredNamespaces.Length == 0 || folderMetadata.ClassFolders.Count == 0)
            {
                WriteComment(GetResourceString(resKeyCommentNoDataFilesFound));
                return;
            }

            int fileCount = 0;
            foreach (var classFolderInfo in folderMetadata.ClassFolders)
            {
                var model = fileService.BuildModel(classFolderInfo, UseFullyQualifiedTypeNames);

                WriteNewListDeclaration(model);

                foreach (var fileInfo in model.ContentFiles)
                {
                    fileCount++;
                    BuildStaticDataContent(fileInfo, model);
                }
            }

            if (fileCount == 0)
            {
                WriteComment(GetResourceString(resKeyCommentNoDataFilesFound));
            }
        }

        #region Protected Helper Methods

        /// <summary>
        /// Write out required using (Imports in VB) statements for all discovered namespaces (ignored if UseFullyQualifiedTypeNames = true)
        /// </summary>
        protected void WriteNamespaceImports()
        {
            if (!UseFullyQualifiedTypeNames)
            {
                foreach (var ns in folderMetadata.DiscoveredNamespaces)
                {
                    WriteUsing(ns);
                }
            }
        }

        /// <summary>
        /// Helper method to push a single indent that is the size of an equivalent number of the specified indents. Ex: If a single indent is a tab, you can push 5 tabs onto the stack by calling PushIndent(5).
        /// A single PopIndent() is used to remove the indent, regardless of the number of indents that are pushed initially.
        /// </summary>
        /// <param name="numberOfIndents"></param>
        protected void PushIndent(int numberOfIndents)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < numberOfIndents; i++)
            {
                sb.Append(SingleIndent);
            }
            PushIndent(sb.ToString());
        }

        /// <summary>
        /// Helper method to write a single opening brace, on its own line.
        /// </summary>
        protected void WriteOpenBrace()
        {
            WriteLine(openBrace);
        }

        /// <summary>
        /// Helper method to write a single closing brace, on its own line.
        /// </summary>
        protected void WriteClosingBrace()
        {
            WriteLine(closeBrace);
        }

        /// <summary>
        /// Helper method to write a single blank line.
        /// </summary>
        protected void WriteBlankLine()
        {
            WriteLine(string.Empty);
        }

        protected string GetDefaultNamespace()
        {
            const string notUsed = "notused";
            return HostProperty.ResolveParameterValue(notUsed, notUsed, projectDefaultNamespace);
        }

        /// <summary>
        /// Helper method to return the default namespace for the current active project
        /// </summary>
        /// <returns></returns>
        protected void WriteDefaultNamespace()
        {
            Write(GetDefaultNamespace());
            return;
        }

        #endregion

        #region Private Methods

        private void BuildStaticDataContent(FileInfo fileInfo, ModelInfo model)
        {
            var content = fileService.ReadFileContent(fileInfo, model.FullName);
            var fileName = Path.GetFileName(content.FilePath);

            WriteBeginRegion(model.OutputName, fileName);

            var adapter = CreateContentAdapter(fileName);
            var dt = ConvertContentToDataTable(content, adapter, model, fileName);
            if (dt == null)
            {
                WriteEndRegion();
                return;
            }

            WriteDataRowExceptionsAsComments(dt);

            var key = string.Concat(model.FullName, colon, fileName);

            try
            {
                WriteNewListInstantiation(model, key);

                int rowIndex = 0;
                bool previousLineWasComment = false;
                while (rowIndex < dt.Rows.Count)
                {
                    var row = dt.Rows[rowIndex];
                    rowIndex++;

                    if (row.RowError == emptyRow)
                    {
                        // allow blank lines in the file
                        WriteBlankLine();
                        previousLineWasComment = false;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(row.RowError))
                        {
                            // insert blank line before each new object instance (unless previous line was a comment)
                            if (!previousLineWasComment)
                            {
                                WriteBlankLine();
                            }

                            WriteNewObject(row, model, fileName, rowIndex + 1, adapter.IgnoreSchemaLibrary);
                            previousLineWasComment = false;
                        }
                        else
                        {
                            WriteBlankLine();
                            WriteComment(row.RowError);
                            previousLineWasComment = true;
                        }
                    }
                }

                if (rowIndex == 0)
                {
                    WriteCommentUnableToGenerateRegion(string.Format(GetResourceString(resKeyCommentEmptyFile), fileName));
                }
            }
            catch (Exception ex)
            {
                WriteCommentUnableToGenerateRegion(string.Format(GetResourceString(resKeyCommentErrorGeneratingList), model.OutputName, fileName, ex.Message));
            }

            WriteEndRegion();
        }

        private void WriteDataRowExceptionsAsComments(DataTable dt)
        {
            var exceptions = dt.ExtendedProperties[dataRowExceptions] as List<string>;
            if (exceptions != null && exceptions.Count > 0)
            {
                foreach (var exception in exceptions)
                {
                    WriteComment(exception);
                    Warning(exception);
                }
            }
        }

        private DataTable ConvertContentToDataTable(DataFileContent content, ContentAdapter adapter, ModelInfo model, string fileName)
        {
            DataTable dt = null;

            try
            {
                dt = adapter.CreateDataTable(content);
            }
            catch (Exception ex)
            {
                WriteCommentUnableToGenerateRegion(string.Format(GetResourceString(resKeyCommentErrorGeneratingList), model.OutputName, fileName, ex.Message));
            }

            return dt;
        }

        private void WriteNewObject(DataRow dr, ModelInfo model, string fileName, int lineNumber, bool ignoreSchemaLibrary)
        {
            var propAssignments = new List<string>();
            var lastNonCommentIndex = -1;

            try
            {
                for (int i = 0; i < dr.Table.Columns.Count; i++)
                {
                    var col = dr.Table.Columns[i];
                    var propValue = dr[col];

                    if (!(bool)col.ExtendedProperties[excludeFromCodeGenProp])
                    {
                        var colError = dr.GetColumnError(col);
                        if (!string.IsNullOrEmpty(colError))
                        {
                            propAssignments.Add(string.Format(GetCodeFromResource(resKeyCommentInitiator), colError));
                            Warning(colError);
                        }
                        else
                        {
                            if (i > 0 && lastNonCommentIndex > -1)
                            {
                                propAssignments[lastNonCommentIndex] += comma;
                            }

                            if (ignoreSchemaLibrary)
                            {
                                if (IsNull(propValue))
                                {
                                    propValue = string.Empty;
                                }
                                propAssignments.Add(string.Format(GetCodeFromResource(resKeyPropAssignment), col.ColumnName, FormatData((string)propValue, lineNumber, fileName)));
                            }
                            else
                            {
                                propAssignments.Add(FormatPropAssignment(col, propValue));
                            }

                            lastNonCommentIndex = propAssignments.Count - 1;
                        }
                    }
                    else
                    {
                        if (!IsNull(propValue))
                        {
                            propAssignments.Add(string.Format(GetCodeFromResource(resKeyCommentInitiator), string.Concat(col.Caption, colon, propValue)));
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Warning(string.Format(GetResourceString(resKeyWarningProcessingRow), lineNumber, fileName, model.OutputName, ex.Message));
            }
            finally
            {
                WriteLine(string.Format(GetCodeFromResource(resKeyCommentInitiator), string.Concat("Row ", lineNumber)));
                if (lastNonCommentIndex > -1)
                {
                    WriteLine(GetAddNewObjectStarterCode(model));
                    WriteLine(openBrace);
                    WriteAllPropertyAssignments(propAssignments);
                    Write(closeObject);
                    WriteLine(GetCodeLineTerminator());
                }
                else
                {
                    // No actual property assignments. Still want to write out any comments, if any.
                    WriteAllPropertyAssignments(propAssignments);
                }

                WriteAllVbComments(propAssignments);    // VB comment lines have to come at the end - can't be interspersed in the middle of the other property assignments
            }
        }

        private void WriteAllPropertyAssignments(List<string> propAssignments)
        {
            foreach (var pa in propAssignments)
            {
                if (CodeLanguage == langConstVB && IsVbComment(pa))
                {
                    continue;
                }
                WriteLine(string.Concat(SingleIndent, pa));
            }
        }

        private void WriteAllVbComments(List<string> propAssignments)
        {
            foreach (var pa in propAssignments)
            {
                if (CodeLanguage == langConstVB && IsVbComment(pa))
                {
                    WriteLine(pa);
                }
            }
        }

        private static bool IsVbComment(string value)
        {
            return value.StartsWith(vbCommentInitiator);
        }

        private string FormatPropAssignment(DataColumn col, object propValue)
        {
            var propType = col.DataType;
            string propTypeName = propType.Name;
            bool isNullable = col.AllowDBNull;

            string formattedValue = GetNullValueIdentifier();

            if (IsNull(propValue))
            {
                if (!isNullable)
                {
                    if (propTypeName == dateType)
                    {
                        formattedValue = GetCodeFromResource(resKeyDateMinValue);
                    }
                    else if (propTypeName != stringType)
                    {
                        formattedValue = string.Concat(propTypeName, dot, minValue);
                    }
                }
            }
            else
            {
                if (propTypeName == stringType)
                {
                    formattedValue = string.Concat(quote, propValue, quote);
                }
                else if (propTypeName == dateType)
                {
                    formattedValue = string.Format(GetCodeFromResource(resKeyDateParse), propValue);
                }
                else
                {
                    formattedValue = string.Format(GetCodeFromResource(resKeyCastValue), propTypeName, propValue);
                }
            }

            return string.Format(GetCodeFromResource(resKeyPropAssignment), col.ColumnName, formattedValue);
        }

        private string FormatData(string data, int lineNumber, string fileName)
        {
            if (string.IsNullOrEmpty(data))
            {
                return GetNullValueIdentifier();
            }
            data = data.Trim();

            if (data.StartsWith(dateMarker))
            {
                if (data.Equals(minDate))
                {
                    return GetCodeFromResource(resKeyDateMinValue);
                }
                if (data.Equals(maxDate))
                {
                    return GetCodeFromResource(resKeyDateMaxValue);
                }

                if (data.Length > 4 && data.EndsWith(dateMarker))
                {
                    if (CodeLanguage.Equals(langConstCS))
                    {
                        // C# must use DateTime.Parse for dates
                        return string.Format(GetCodeFromResource(resKeyDateParse), data.Replace(dateMarker, string.Empty));
                    }
                    else
                    {
                        //  VB can use the date literals as is
                        return data;
                    }
                }

                // invalid format - return MinValue as a fallback
                Warning(string.Format(GetResourceString(resKeyWarningInvalidDateFormat), data, lineNumber, fileName));
                return GetCodeFromResource(resKeyDateMinValue);
            }

            if (data.StartsWith(quote) && !data.EndsWith(quote))
            {
                WarningMissingQuote(lineNumber, fileName);
                return string.Concat(data, quote);
            }
            if (data.EndsWith(quote) && !data.StartsWith(quote))
            {
                WarningMissingQuote(lineNumber, fileName);
                return string.Concat(quote, data);
            }

            return data;
        }

        private static bool IsNull(object propValue)
        {
            return propValue == null || propValue == DBNull.Value || propValue.ToString().Equals(nullValue, StringComparison.InvariantCultureIgnoreCase);
        }

        private void WarningMissingQuote(int lineNumber, string fileName)
        {
            Warning(string.Format(GetResourceString(resKeyWarningMissingQuote), lineNumber, fileName));
        }

        private void WriteBeginRegion(string typeName, string fileName)
        {
            string comment = string.Format(GetResourceString(resKeyCommentPopulateList), typeName, fileName);
            WriteBlankLine();
            WriteBeginRegion(comment);
            WriteBlankLine();
        }

        private void WriteCommentUnableToGenerateRegion(string warningText)
        {
            WriteComment(GetResourceString(resKeyCommentUnableToGenerateRegion));
            if (!string.IsNullOrWhiteSpace(warningText))
            {
                WriteComment(warningText);
                Warning(warningText);
            }
        }

        private void WriteUsing(string ns)
        {
            var text = string.Format(GetCodeFromResource(resKeyUsing), ns);
            WriteLine(text);
        }

        private void WriteBeginRegion(string comment)
        {
            var text = string.Format(GetCodeFromResource(resKeyBeginRegion), comment);
            WriteLine(text);
        }

        private void WriteEndRegion()
        {
            var text = GetCodeFromResource(resKeyEndRegion);
            WriteBlankLine();
            WriteLine(text);
        }

        private void WriteComment(string comment)
        {
            var text = string.Format(GetCodeFromResource(resKeyCommentInitiator), comment);
            WriteLine(text);
        }

        private void WriteNewListDeclaration(ModelInfo model)
        {
            var text = string.Format(GetCodeFromResource(resKeyNewListDeclaration), model.OutputName, model.LocalVariableName);
            WriteBlankLine();
            WriteLine(text);
        }

        private void WriteNewListInstantiation(ModelInfo model, string key)
        {
            var text = string.Format(GetCodeFromResource(resKeyNewListInstantiation), model.LocalVariableName, model.OutputName);
            WriteLine(text);
            text = string.Format(GetCodeFromResource(resKeyAddListLibrary), key, model.LocalVariableName);
            WriteLine(text);
        }

        private string GetAddNewObjectStarterCode(ModelInfo model)
        {
            var text = string.Format(GetCodeFromResource(resKeyAddNewObjectStarterCode), model.LocalVariableName, model.OutputName);
            return text;
        }

        private string GetCodeLineTerminator()
        {
            return GetCodeFromResource(resKeyCodeLineTerminator);
        }

        private string GetNullValueIdentifier()
        {
            return GetCodeFromResource(resKeyNullValueIdentifier);
        }

        private FileService CreateFileService()
        {
            var rootFolder = new DirectoryInfo(HostProperty.ResolvePath(string.Empty));   // Gets path where this text template file resides
            var fileService = new FileService(rootFolder);
            return fileService;
        }

        private ContentAdapter CreateContentAdapter(string fileName)
        {
            ContentAdapter adapter = null;
            if (Path.GetExtension(fileName).Equals(csvExtension, StringComparison.InvariantCultureIgnoreCase) && !UseVsCodeModelForCsvFiles)
            {
                adapter = new ContentAdapter(codeDomProvider);
            }
            else
            {
                adapter = new ContentAdapter(codeDomProvider, schemaLibrary);
            }

            return adapter;
        }

        private ITextTemplatingEngineHost HostProperty
        {
            get
            {
                if (_host == null)
                {
                    var hostProp = this.GetType().GetProperty(host);
                    if (hostProp != null)
                    {
                        _host = (ITextTemplatingEngineHost)hostProp.GetValue(this, null);
                    }
                    else
                    {
                        Error(CoreResources.ErrorHostNotAvail);
                    }
                }
                return _host;
            }
        }

        private Project HostProject
        {
            get
            {
                if (_project == null)
                {
                    var vs = (HostProperty as IServiceProvider).GetService(typeof(DTE)) as DTE;

                    if (vs != null)
                    {
                        _project = vs.Solution.FindProjectItem(HostProperty.TemplateFile)
                            .ContainingProject as Project;
                    }
                }

                return _project;
            }
        }

        private ResourceManager ResourceManager
        {
            get
            {
                const string baseName = "PSS.TextTemplating.Resources";
                if (object.ReferenceEquals(resourceManager, null))
                {
                    ResourceManager temp = new ResourceManager(baseName, typeof(StaticDataLibraryGenerator).Assembly);
                    resourceManager = temp;
                }
                return resourceManager;
            }
        }

        private string GetCodeFromResource(string key)
        {
            return ResourceManager.GetString(string.Concat(CodeLanguage, dot, key));
        }
        private string GetResourceString(string key)
        {
            return ResourceManager.GetString(key);
        }
    }

    #endregion
}
