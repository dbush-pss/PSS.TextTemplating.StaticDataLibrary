﻿using System.IO;
using PSS.TextTemplating.Core;

namespace PSS.TextTemplating.IO
{
    /// <summary>
    /// File reader that consumes simple CSV files and transforms the content into a common data structure for code-generation
    /// </summary>
    public class CsvFileReader : IFileReader
    {
        private const char delimiter = ',';
        private const string multiLineCommentStarter = "/*";
        private const string multiLineCommentCloser = "*/";
        private const string singleLineCommentStarter = "//";

        /// <summary>
        /// Reads all content in the specified input file and parses the file into a collection of column headers and data rows
        /// </summary>
        /// <param name="stream">Input stream that is already open and ready for reading</param>
        /// <param name="inputFilePath">The full path to the input file</param>
        /// <param name="modelFullName">The fully qualified class name of the object represented by the data in the input file</param>
        /// <returns>DataFileContent representing all data in the file</returns>
        public DataFileContent ReadAllContent(Stream stream, string filePath, string modelFullName)
        {
            var content = new DataFileContent(filePath, modelFullName);
            using (StreamReader reader = new StreamReader(stream))
            {
                if (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        var fieldNames = line.Split(delimiter);
                        for (int i = 0; i < fieldNames.Length; i++)
                        {
                            content.ColumnHeaders[i] = fieldNames[i];
                        }

                        while (!reader.EndOfStream)
                        {
                            line = reader.ReadLine();
                            var dataValues = new DataValues();

                            // blank lines will be added with an empty DataValues collection
                            if (!string.IsNullOrWhiteSpace(line))
                            {
                                // comments are assigned to the CommentText property
                                bool isMultiLineCommentBlock = line.TrimStart().StartsWith(multiLineCommentStarter);
                                if (isMultiLineCommentBlock || line.TrimStart().StartsWith(singleLineCommentStarter))
                                {
                                    if (isMultiLineCommentBlock)
                                    {
                                        dataValues.CommentText = line.Replace(multiLineCommentStarter, string.Empty)
                                                                     .Replace(multiLineCommentCloser, string.Empty);
                                    }
                                    else
                                    {
                                        dataValues.CommentText = line.Replace(singleLineCommentStarter, string.Empty);
                                    }
                                }
                                else
                                {
                                    // split the row data into the DataValues collection
                                    dataValues.FromArray(line.Split(delimiter));
                                }
                            }

                            content.Rows.Add(dataValues);
                        }
                    }
                    else
                    {
                        content.ColumnHeaders = null;
                    }
                }
                else
                {
                    content.ColumnHeaders = null;
                }
            }

            return content;
        }
    }
}
