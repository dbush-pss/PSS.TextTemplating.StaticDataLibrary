﻿using System;
using System.IO;
using Excel;
using PSS.TextTemplating.Core;

namespace PSS.TextTemplating.IO
{
    /// <summary>
    /// File reader that consumes Excel spreadsheets and transforms the content into a common data structure for code-generation
    /// </summary>
    public class ExcelFileReader : IFileReader
    {
        private const string column = "Column";
        private const string commentMarker = "**";

        /// <summary>
        /// Reads all content in the specified input file and parses the file into a collection of column headers and data rows
        /// </summary>
        /// <param name="stream">Input stream that is already open and ready for reading</param>
        /// <param name="inputFilePath">The full path to the input file</param>
        /// <param name="modelFullName">The fully qualified class name of the object represented by the data in the input file</param>
        /// <returns>DataFileContent representing all data in the file</returns>
        public DataFileContent ReadAllContent(Stream stream, string filePath, string modelFullName)
        {
            var content = new DataFileContent(filePath, modelFullName);
            int lineNumber = 0;

            using (IExcelDataReader reader = CreateExcelReader(stream, filePath))
            {
                while (reader.Read())
                {
                    lineNumber++;
                    if (lineNumber == 1)
                    {
                        PopulateColumnHeaders(content, reader);

                        if (content.ColumnHeaders.Count == 0)
                        {
                            content.ColumnHeaders = null;
                            break;
                        }
                    }
                    else
                    {
                        var dataValues = new DataValues();

                        if (reader.FieldCount > 1 && GetValue(reader, 0).Equals(commentMarker, StringComparison.InvariantCultureIgnoreCase))
                        {
                            dataValues.CommentText = GetValue(reader, 1);
                        }
                        else
                        {
                            bool isEmptyRow = true;
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                var value = GetValue(reader, i);
                                if (value.Length > 0)
                                {
                                    isEmptyRow = false;
                                }
                                dataValues[i] = GetValue(reader, i);
                            }

                            if (dataValues.Count > 0 && isEmptyRow)
                            {
                                // if all cells are empty, treat as a blank row for the purpose of code generation
                                dataValues.Clear();
                            }
                        }

                        content.Rows.Add(dataValues);
                    }
                }

                if (lineNumber == 0)
                {
                    content.ColumnHeaders = null;
                }
            }

            return content;
        }

        private static void PopulateColumnHeaders(DataFileContent content, IExcelDataReader reader)
        {
            int colIndex = 0;
            while (colIndex < reader.FieldCount)
            {
                string colName = GetValue(reader, colIndex); ;
                content.ColumnHeaders[colIndex] = string.IsNullOrWhiteSpace(colName) ? string.Concat(column, colIndex + 1) : colName;
                colIndex++;
            }
        }

        private static string GetValue(IExcelDataReader reader, int colIndex)
        {
            return reader.GetString(colIndex) ?? string.Empty;
        }

        private static IExcelDataReader CreateExcelReader(Stream stream, string filePath)
        {
            if (filePath.EndsWith(".xls", StringComparison.InvariantCultureIgnoreCase))
            {
                return ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (filePath.EndsWith(".xlsx", StringComparison.InvariantCultureIgnoreCase))
            {
                return ExcelReaderFactory.CreateOpenXmlReader(stream);
            }
            else
            {
                throw new Exception(CoreResources.ErrorInvalidInputFilename);
            }

        }


    }
}
