﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PSS.TextTemplating.Core;

namespace PSS.TextTemplating.IO
{
    public class FileService
    {
        private const string csvFileSearchPattern = "*.csv";
        private const string xlFileSearchPattern = "*.xls*";
        private const string xlsExtension = ".xls";

        private readonly DirectoryInfo rootFolder;
        private List<DirectoryInfo> classFolders = new List<DirectoryInfo>();

        public FileService(DirectoryInfo rootFolder)
        {
            this.rootFolder = rootFolder;
        }

        public FolderMetadata GetFolderMetadata()
        {
            var namespaceFolders = rootFolder.GetDirectories();

            var namespaces = namespaceFolders.Select(f => f.Name).ToArray();
            foreach (var folder in namespaceFolders)
            {
                classFolders.AddRange(folder.GetDirectories());
            }

            return new FolderMetadata(namespaces, classFolders);
        }

        public ModelInfo BuildModel(DirectoryInfo classFolder, bool useFullyQualifiedTypeNames)
        {
            var csvFiles = classFolder.GetFiles(csvFileSearchPattern, SearchOption.AllDirectories);
            var xlFiles = classFolder.GetFiles(xlFileSearchPattern, SearchOption.AllDirectories);
            var allFiles = csvFiles.Concat(xlFiles);

            ModelInfo model = new ModelInfo(classFolder.Parent.Name, classFolder.Name, useFullyQualifiedTypeNames, allFiles);

            return model;
        }

        public DataFileContent ReadFileContent(FileInfo fileInfo, string modelFullName)
        {
            using (FileStream fs = File.Open(fileInfo.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                var fileReader = CreateFileReader(fileInfo);
                return fileReader.ReadAllContent(fs, fileInfo.FullName, modelFullName);
            }
        }

        private static IFileReader CreateFileReader(FileInfo fileInfo)
        {
            if (fileInfo.Extension.StartsWith(xlsExtension, StringComparison.InvariantCultureIgnoreCase))
            {
                return new ExcelFileReader();
            }
            else
            {
                return new CsvFileReader();
            }
        }

    }
}
