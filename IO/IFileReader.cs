﻿using System.IO;
using PSS.TextTemplating.Core;

namespace PSS.TextTemplating.IO
{
    public interface IFileReader
    {
        /// <summary>
        /// Reads all content in the specified input file and parses the file into a collection of column headers and data rows
        /// </summary>
        /// <param name="stream">Input stream that is already open and ready for reading</param>
        /// <param name="inputFilePath">The full path to the input file</param>
        /// <param name="modelFullName">The fully qualified class name of the object represented by the data in the input file</param>
        /// <returns>DataFileContent representing all data in the file</returns>
        DataFileContent ReadAllContent(Stream stream, string inputFilePath, string modelFullName);
    }
}
